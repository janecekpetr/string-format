// Copyright (c) 2004-2022 QOS.ch, Petr Janeček
// All rights reserved.
//
// Permission is hereby granted, free  of charge, to any person obtaining
// a  copy  of this  software  and  associated  documentation files  (the
// "Software"), to  deal in  the Software without  restriction, including
// without limitation  the rights to  use, copy, modify,  merge, publish,
// distribute,  sublicense, and/or sell  copies of  the Software,  and to
// permit persons to whom the Software  is furnished to do so, subject to
// the following conditions:
//
// The  above  copyright  notice  and  this permission  notice  shall  be
// included in all copies or substantial portions of the Software.
//
// THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
// EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
// MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.gitlab.janecekpetr.stringformat;

import static java.util.stream.Collectors.joining;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.IntStream;

import javax.annotation.processing.Generated;
import javax.lang.model.element.Modifier;

public class Generate {

    private static final int DEFAULT_NUMBER_OF_ARGS_FOR_EXACT_FORMAT = 4;

    private static final ClassName STRING_TYPE = ClassName.get(String.class);

    private static final Set<TypeName> ALL_ARG_TYPES = ImmutableSet.of(
            TypeName.OBJECT, STRING_TYPE, TypeName.INT, TypeName.LONG, TypeName.DOUBLE,
            TypeName.BYTE, TypeName.CHAR, TypeName.BOOLEAN, TypeName.FLOAT, TypeName.SHORT);

    private static final Set<TypeName> ARG_TYPES_FOR_FORMAT_METHOD = ImmutableSet.of(
            TypeName.OBJECT, STRING_TYPE, TypeName.INT, TypeName.LONG, TypeName.DOUBLE);

    private static final int NUMBER_TOSTRING_LENGTH_GUESS = 10;
    private static final int OBJECT_TOSTRING_LENGTH_GUESS = 16;

    private final TypeSpec.Builder classBuilder;

    private final FieldSpec constNumberToStringLengthGuess = FieldSpec.builder(int.class, "NUMBER_TOSTRING_LENGTH_GUESS")
        .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
        .initializer("$L", NUMBER_TOSTRING_LENGTH_GUESS)
        .build();

    private final FieldSpec constObjectToStringLengthGuess = FieldSpec.builder(int.class, "OBJECT_TOSTRING_LENGTH_GUESS")
        .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
        .initializer("$L", OBJECT_TOSTRING_LENGTH_GUESS)
        .build();

    private final FieldSpec fieldMessageParts = FieldSpec.builder(String[].class, "messageParts")
        .addModifiers(Modifier.PRIVATE, Modifier.FINAL)
        .build();
    private final FieldSpec fieldLength = FieldSpec.builder(int.class, "length")
        .addModifiers(Modifier.PRIVATE, Modifier.FINAL)
        .build();

    public static void main(String... args) throws IOException {
        int maxNumberOfArgsForExactFormat = DEFAULT_NUMBER_OF_ARGS_FOR_EXACT_FORMAT;
        if (args.length > 0) {
            maxNumberOfArgsForExactFormat = Integer.parseInt(args[0]);
        }
        JavaFile javaFile = new Generate("CompiledStringFormat").run(maxNumberOfArgsForExactFormat);
        javaFile.writeTo(Paths.get("./target/generated-sources/java"));
    }

    public static Iterable<JavaFile> generate(String name) throws IOException {
        JavaFile generatedFile = new Generate(name).run(DEFAULT_NUMBER_OF_ARGS_FOR_EXACT_FORMAT);
        return List.of(generatedFile);
    }

    private Generate(String name) {
        classBuilder = TypeSpec.classBuilder(name).addModifiers(Modifier.PUBLIC, Modifier.FINAL);
    }

    private JavaFile run(int maxNumberOfArgsForExactFormat) throws IOException {
        addClassAnnotation();

        addConstants();
        addFields();
        addConstructor();

        addVarArgFormattingMethods(maxNumberOfArgsForExactFormat);
        addExactFormattingMethods(maxNumberOfArgsForExactFormat);

        return buildJavaFile(classBuilder.build());
    }

    private void addClassAnnotation() {
        classBuilder.addAnnotation(AnnotationSpec.builder(Generated.class)
            .addMember("value", "$S", "com.squareup.javapoet:1.13.0")
            .addMember("date", "$S", OffsetDateTime.now())
            .build());
    }

    private void addConstants() {
        classBuilder.addField(constNumberToStringLengthGuess);
        classBuilder.addField(constObjectToStringLengthGuess);
    }

    private void addFields() {
        classBuilder.addField(fieldMessageParts);
        classBuilder.addField(fieldLength);
    }

    private void addConstructor() {
        classBuilder.addMethod(MethodSpec.constructorBuilder()
            .addParameter(fieldMessageParts.type, fieldMessageParts.name)
            .addParameter(fieldLength.type, fieldLength.name)
            .addStatement("this.$1N = $1N", fieldMessageParts)
            .addStatement("this.$1N = $1N", fieldLength)
            .build());
    }

    private void addVarArgFormattingMethods(int maxNumberOfArgsForExactFormat) {
        List<MethodSpec> formatMethods = new ArrayList<>();
        List<MethodSpec> appendMethods = new ArrayList<>();
        for (TypeName argType : ALL_ARG_TYPES) {
            MethodSpec formatToVarArgMethod = buildFormatToVarArgMethod(argType);
            MethodSpec formatVarArgMethod = buildFormatVarArgMethod(argType, maxNumberOfArgsForExactFormat, formatToVarArgMethod);
            formatMethods.add(formatVarArgMethod);
            appendMethods.add(formatToVarArgMethod);
        }
        classBuilder.addMethods(formatMethods);
        classBuilder.addMethods(appendMethods);
    }

    private void addExactFormattingMethods(int maxNumberOfArgsForFormat) {
        List<MethodSpec> formatMethods = new ArrayList<>();
        List<MethodSpec> appendMethods = new ArrayList<>();
        for (List<TypeName> argTypes : allCombinationsOfTypes(ARG_TYPES_FOR_FORMAT_METHOD, maxNumberOfArgsForFormat)) {
            formatMethods.add(buildFormatExactMethod(argTypes));
            appendMethods.add(buildFormatToExactMethod(argTypes));
        }
        classBuilder.addMethods(formatMethods);
        classBuilder.addMethods(appendMethods);
    }

    private static List<List<TypeName>> allCombinationsOfTypes(Set<TypeName> types, int maxNumberOfArgsForFormat) {
        List<List<TypeName>> allArgVariants = new ArrayList<>();
        List<Set<TypeName>> argVariants = new ArrayList<>();
        for (int i = 0; i < maxNumberOfArgsForFormat; i++) {
            argVariants.add(types);
            allArgVariants.addAll(Sets.cartesianProduct(argVariants));
        }
        return allArgVariants;
    }

    private MethodSpec buildFormatVarArgMethod(TypeName argClass, int maxNumberOfArgsForExactFormat, MethodSpec formatToVarArgMethod) {
        //	public String format(int... args)
        ParameterSpec argsParameter = ParameterSpec.builder(ArrayTypeName.of(argClass), "args").build();
        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("format")
            .addModifiers(Modifier.PUBLIC)
            .returns(String.class)
            .addParameter(argsParameter)
            .varargs();

        addArgCountPreconditionCheck(methodBuilder, argsParameter);

        // TODO only do this for ARG_TYPES_FOR_FORMAT_METHOD types
        //  switch (argCount) {
        methodBuilder.beginControlFlow("switch (argCount)");

        //  case 1: return format(args[0]);
        //  case 2: return format(args[0], args[1]);
        //  case 3: return format(args[0], args[1], args[2]);
        //  case 4: return format(args[0], args[1], args[2], args[3]);
        for (int i = 1; i <= maxNumberOfArgsForExactFormat; i++) {
            CodeBlock exactArgs = CodeBlock.of(
                    IntStream.range(0, i)
                        .mapToObj(index -> "$1N[" + index + "]")
                        .collect(joining(", ")),
                    argsParameter
            );
            methodBuilder.addStatement("case $L: return format($L)", i, exactArgs);
        }
        //  default: {
        //      StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
        //      formatTo(builder, args);
        //      return builder.toString();
        //  }
        methodBuilder.beginControlFlow("default:");
        methodBuilder.addStatement(
                "$1T builder = new $1T($2N + argCount*$3N)",
                StringBuilder.class,
                fieldLength,
                argClass.isPrimitive() ? constNumberToStringLengthGuess : constObjectToStringLengthGuess
                );
        methodBuilder.addStatement("$N(builder, $N)", formatToVarArgMethod, argsParameter);
        methodBuilder.addStatement("return builder.toString()");
        methodBuilder.endControlFlow();

        methodBuilder.endControlFlow();

        return methodBuilder.build();
    }

    private MethodSpec buildFormatToVarArgMethod(TypeName argClass) {
        //	public StringBuilder formatTo(StringBuilder stringBuilder, int... args)
        ParameterSpec stringBuilderParameter = ParameterSpec.builder(StringBuilder.class, "stringBuilder").build();
        ParameterSpec argsParameter = ParameterSpec.builder(ArrayTypeName.of(argClass), "args").build();
        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("formatTo")
            .addModifiers(Modifier.PUBLIC)
            .returns(StringBuilder.class)
            .addParameter(stringBuilderParameter)
            .addParameter(argsParameter)
            .varargs();

        addArgCountPreconditionCheck(methodBuilder, argsParameter);

        //	for (int i = 0; i < argCount; i++) {
        //		stringBuilder.append(messageParts[i]);
        //		stringBuilder.append(args[i]);
        //	}
        //	stringBuilder.append(messageParts[argsCount]);
        methodBuilder.beginControlFlow("for (int i = 0; i < argCount; i++)");
        methodBuilder.addStatement("$N.append($N[i])", stringBuilderParameter, fieldMessageParts);
        if (isPrimitiveOrString(argClass)) {
            methodBuilder.addStatement("$N.append($N[i])", stringBuilderParameter, argsParameter);
        } else {
            methodBuilder.addStatement("$T.deepAppendObject($N, $N[i])", ToStrings.class, stringBuilderParameter, argsParameter);
        }
        methodBuilder.endControlFlow();
        methodBuilder.addStatement("$N.append($N[argCount])", stringBuilderParameter, fieldMessageParts);

        methodBuilder.addStatement("return $N", stringBuilderParameter);
        return methodBuilder.build();
    }

    private void addArgCountPreconditionCheck(MethodSpec.Builder methodBuilder, ParameterSpec argsParameter) {
        //  final int argCount = args.length;
        methodBuilder.addStatement("final int argCount = $N.length", argsParameter);

        //  if (messageParts.length != (argCount + 1))
        //  throw new IllegalArgumentException("Incorrect number of arguments, "
        //          + "message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
        methodBuilder.beginControlFlow("if ($N.length != (argCount + 1))", fieldMessageParts);
        methodBuilder.addStatement(
                "throw new $1T(\"Incorrect number of arguments, message parts: \" + $2T.toString($3N) + \", args: \" + $2T.toString($4N))",
                IllegalArgumentException.class, Arrays.class, fieldMessageParts, argsParameter);
        methodBuilder.endControlFlow();
    }

    private MethodSpec buildFormatExactMethod(List<TypeName> argClasses) {
        //	public String format(int arg0, int arg1)
        final int argCount = argClasses.size();
        List<ParameterSpec> args = generateArgs(argClasses, argCount);

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("format")
            .addModifiers(Modifier.PUBLIC)
            .addParameters(args)
            .returns(String.class);

        addArgCountPreconditionCheck(methodBuilder, args, argCount);

        //	return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
        CodeBlock.Builder returnStatementBuilder = CodeBlock.builder();
        returnStatementBuilder.add("return ");
        for (int i = 0; i < argCount; i++) {
            returnStatementBuilder.add("$N[$L]", fieldMessageParts, i);
            returnStatementBuilder.add(" + ");

            ParameterSpec arg = args.get(i);
            if (isPrimitiveOrString(arg.type)) {
                returnStatementBuilder.add("$N", arg);
            } else {
                returnStatementBuilder.add("$T.deepToString($N)", ToStrings.class, arg);
            }
            returnStatementBuilder.add(" + ");
        }
        returnStatementBuilder.add("$N[$L]", fieldMessageParts, argCount);
        methodBuilder.addStatement(returnStatementBuilder.build());

        return methodBuilder.build();
    }

    private MethodSpec buildFormatToExactMethod(List<TypeName> argClasses) {
        ParameterSpec stringBuilderParameter = ParameterSpec.builder(StringBuilder.class, "stringBuilder").build();

        final int argCount = argClasses.size();
        List<ParameterSpec> args = generateArgs(argClasses, argCount);

        //	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1)
        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("formatTo")
            .addModifiers(Modifier.PUBLIC)
            .returns(StringBuilder.class)
            .addParameter(stringBuilderParameter)
            .addParameters(args);

        addArgCountPreconditionCheck(methodBuilder, args, argCount);

        //	stringBuilder.append(messageParts[0]);
        //	stringBuilder.append(arg0);
        //	stringBuilder.append(messageParts[1]);
        for (int i = 0; i < argCount; i++) {
            methodBuilder.addStatement("$N.append($N[$L])", stringBuilderParameter, fieldMessageParts, i);
            ParameterSpec arg = args.get(i);
            if (isPrimitiveOrString(arg.type)) {
                methodBuilder.addStatement("$N.append($N)", stringBuilderParameter, arg);
            } else {
                methodBuilder.addStatement("$T.deepAppendObject($N, $N)", ToStrings.class, stringBuilderParameter, arg);
            }
        }
        methodBuilder.addStatement("$N.append($N[$L])", stringBuilderParameter, fieldMessageParts, argCount);

        methodBuilder.addStatement("return $N", stringBuilderParameter);
        return methodBuilder.build();
    }

    private void addArgCountPreconditionCheck(MethodSpec.Builder methodBuilder, List<ParameterSpec> args, int argCount) {
        //	if (messageParts.length != 3)
        //		throw new IllegalArgumentException("Incorrect number of arguments, message parts: "
        //				+ Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
        methodBuilder.beginControlFlow("if ($N.length != $L)", fieldMessageParts, argCount + 1);
        methodBuilder.addStatement(
                "throw new $T(\"Incorrect number of arguments, message parts: \" + $T.toString($N) + \", args: $L\")",
                IllegalArgumentException.class, Arrays.class, fieldMessageParts, argsToString(args));
        methodBuilder.endControlFlow();
    }

    private static List<ParameterSpec> generateArgs(List<TypeName> argClasses, int argCount) {
        return IntStream.range(0, argCount)
            .mapToObj(i -> {
                TypeName argType = argClasses.get(i);
                String argName = "arg" + i;
                return ParameterSpec.builder(argType, argName).build();
            })
            .toList();
    }

    //	[" + arg0 + ", " + arg1 + "]
    private static String argsToString(List<ParameterSpec> args) {
        // intentionally omitting leading and trailing quote
        StringJoiner joiner = new StringJoiner(" + \", \" + ", "[\" + ", " + \"]");
        for (ParameterSpec arg : args) {
            joiner.add(arg.name);
        }
        return joiner.toString();
    }

    private static boolean isPrimitiveOrString(TypeName argClass) {
        return argClass.isPrimitive() || argClass.equals(STRING_TYPE);
    }

    private static JavaFile buildJavaFile(TypeSpec stringFormatClass) throws IOException {
        return JavaFile.builder(Generate.class.getPackageName(), stringFormatClass)
            .skipJavaLangImports(true)
            .indent("\t")
            .addFileComment(Resources.toString(Resources.getResource("LICENSE"), StandardCharsets.UTF_8))
            .build();
    }

}
