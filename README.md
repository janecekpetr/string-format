Fast String Formatting
======================

A String formatting library that can precompile its formatting strings and therefore achieve great speed. Created because of [this Stack Overflow question](http://stackoverflow.com/questions/29747912).

Usage
-----

There are two modes of operation.

- The "classic" one without any compilation:

        StringFormat.format("Hello, {}!", "world")

    returns the string `"Hello, world!"`

    This is directly comparable to [slf4j](http://slf4j.org/api/org/slf4j/helpers/MessageFormatter.html) (which is what all this code is based upon) or [log4j](http://logging.apache.org/log4j/2.x/log4j-api/apidocs/org/apache/logging/log4j/message/ParameterizedMessage.html) log message formatting, nothing very fancy is going on under the hood, just the API is more direct and accessible.

- The _compiled_ one where you precompile your formatting template, and can reuse it many, many times later on to get a nice speed boost.

        CompiledStringFormat format = StringFormat.compile("Hello, {}!");
        
        // later on, in a tight loop
        format.format("world")

As of now, everything must be explicit, there's no hidden cleverness. E.g. the static `format()` method could take a look into some sort of cache to see whether the same format template hasn't already been compiled, but it doesn't.

The problem
-----------

See [the original Stack Overflow question](http://stackoverflow.com/questions/29747912). The problem is formatting millions of Strings and realizing that it's horrendously slow. When you use e.g. slf4j's internal formatting utility, or a custom class, it gets much better, but we can still be faster and more generic.

See RESULTS.txt for some benchmarks numbers comparing various String formatting methods.

Building, code generation
-------------------------

The many methods of `CompiledStringFormat` are generated. The repo contains a default implementation which has various `format()` methods with up to 4 different arguments (`Object`, `int`, `long`, `double`). To regenerate this with different settings, either run `Generate` class's `main()` method with a single numeric argument (specifies up to how many arguments it should generate methods), or run the maven phase `generate-sources` with the `Generate` class changed (e.g. to change the default argument types).

TODO
----

- custom `appendIterable()` for `Iterable` instances?