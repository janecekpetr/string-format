// Copyright (c) 2004-2022 QOS.ch, Petr Janeček
// All rights reserved.
//
// Permission is hereby granted, free  of charge, to any person obtaining
// a  copy  of this  software  and  associated  documentation files  (the
// "Software"), to  deal in  the Software without  restriction, including
// without limitation  the rights to  use, copy, modify,  merge, publish,
// distribute,  sublicense, and/or sell  copies of  the Software,  and to
// permit persons to whom the Software  is furnished to do so, subject to
// the following conditions:
//
// The  above  copyright  notice  and  this permission  notice  shall  be
// included in all copies or substantial portions of the Software.
//
// THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
// EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
// MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.gitlab.janecekpetr.stringformat;

import java.util.Arrays;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

class ToStrings {

	private ToStrings() {
		// a static utility class
	}

	private record ObjectArrayWrapper(Object[] objects) {
        @Override
        public String toString() {
            return Arrays.toString(objects);
        }
    }

   private record IntArrayWrapper(int[] ints) {
        @Override
        public String toString() {
            return Arrays.toString(ints);
        }
    }

    private record LongArrayWrapper(long[] longs) {
        @Override
        public String toString() {
            return Arrays.toString(longs);
        }
    }

    private record DoubleArrayWrapper(double[] doubles) {
        @Override
        public String toString() {
            return Arrays.toString(doubles);
        }
    }

    private record ByteArrayWrapper(byte[] bytes) {
        @Override
        public String toString() {
            return Arrays.toString(bytes);
        }
    }

    private record CharArrayWrapper(char[] chars) {
        @Override
        public String toString() {
            return Arrays.toString(chars);
        }
    }

    private record BooleanArrayWrapper(boolean[] booleans) {
        @Override
        public String toString() {
            return Arrays.toString(booleans);
        }
    }

    private record ShortArrayWrapper(short[] shorts) {
        @Override
        public String toString() {
            return Arrays.toString(shorts);
        }
    }

    private record FloatArrayWrapper(float[] floats) {
        @Override
        public String toString() {
            return Arrays.toString(floats);
        }
    }

    // TODO rename and add rationale, this actually generates less garbage in "a" + deepToString(object) situations
    public static Object deepToString(Object messageArg) {
        if (messageArg == null) {
            return "null";
        }

        if (!messageArg.getClass().isArray()) {
            return messageArg;
        }

        if (messageArg instanceof Object[] objects) {
            return new ObjectArrayWrapper(objects);
        }
        if (messageArg instanceof int[] ints) {
            return new IntArrayWrapper(ints);
        } else if (messageArg instanceof long[] longs) {
            return new LongArrayWrapper(longs);
        } else if (messageArg instanceof double[] doubles) {
            return new DoubleArrayWrapper(doubles);
        } else if (messageArg instanceof byte[] bytes) {
            return new ByteArrayWrapper(bytes);
        } else if (messageArg instanceof char[] chars) {
            return new CharArrayWrapper(chars);
        } else if (messageArg instanceof boolean[] booleans) {
            return new BooleanArrayWrapper(booleans);
        } else if (messageArg instanceof short[] shorts) {
            return new ShortArrayWrapper(shorts);
        } else {
            return new FloatArrayWrapper((float[])messageArg);
        }
    }

    public static void deepAppendObject(StringBuilder stringBuilder, Object messageArg) {
		deepAppendObject(stringBuilder, messageArg, null);
    }

    private static void deepAppendObject(StringBuilder stringBuilder, Object messageArg, Set<Object[]> seenSet) {
        if (messageArg == null) {
        	stringBuilder.append("null");
            return;
        }

        if (!messageArg.getClass().isArray()) {
            stringBuilder.append(messageArg.toString());	// using toString() directly for speed, it's safe as we already handled null
        	return;
        }

        stringBuilder.append('[');
        if (messageArg instanceof Object[] objects) {
            appendObjectArray(
                    stringBuilder,
                    objects,
                    (seenSet != null) ? seenSet : Collections.newSetFromMap(new IdentityHashMap<>())
            );
        } else if (messageArg instanceof int[] ints) {
        	appendArray(stringBuilder, ints);
        } else if (messageArg instanceof long[] longs) {
        	appendArray(stringBuilder, longs);
        } else if (messageArg instanceof double[] doubles) {
        	appendArray(stringBuilder, doubles);
        } else if (messageArg instanceof byte[] bytes) {
            appendArray(stringBuilder, bytes);
        } else if (messageArg instanceof char[] chars) {
            appendArray(stringBuilder, chars);
        } else if (messageArg instanceof boolean[] booleans) {
        	appendArray(stringBuilder, booleans);
        } else if (messageArg instanceof short[] shorts) {
            appendArray(stringBuilder, shorts);
        } else {
            appendArray(stringBuilder, (float[])messageArg);
        }
        stringBuilder.append(']');
    }

    private static void appendObjectArray(StringBuilder stringBuilder, Object[] objects, Set<Object[]> seenSet) {
    	if (objects.length == 0) {
			return;
		}

    	boolean added = seenSet.add(objects);
		if (added) {
			final int max = objects.length - 1;
			for (int i = 0; i < max; i++) {
				deepAppendObject(stringBuilder, objects[i], seenSet);
				stringBuilder.append(", ");
			}
			deepAppendObject(stringBuilder, objects[max], seenSet);
			seenSet.remove(objects);	// allow repeats in siblings
		} else {
			// a cycle in the array
		    stringBuilder.append("...");
		}
    }

	private static void appendArray(StringBuilder stringBuilder, int[] array) {
	    if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, long[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, double[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, byte[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, char[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, boolean[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, short[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}

	private static void appendArray(StringBuilder stringBuilder, float[] array) {
    	if (array.length == 0) {
			return;
		}

		final int max = array.length - 1;
		for (int i = 0; i < max; i++) {
			stringBuilder.append(array[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(array[max]);
	}
}