package com.gitlab.janecekpetr.stringformat;

import com.google.common.base.Splitter;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Fork(1)
@Warmup(iterations = 2, time = 3, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 6, timeUnit = TimeUnit.SECONDS)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class FormatStringCompilationBenchmark {

	private Pattern placeholderPattern;
	private Splitter placeholderSplitter;

	@Setup
	public void setup(ScreenSleepDisablingBenchmark screenSleepDisabler) {
	    placeholderPattern = Pattern.compile("\\{\\}");
        placeholderSplitter = Splitter.on("{}");
	}

    @Benchmark
    public CompiledStringFormat fastFormatCompile() {
        return StringFormat.compile("Error found while compiling: {}-{}");
    }

	@Benchmark
    public CompiledStringFormat fastFormatCompileByPatternSplit() {
        String[] messageParts = placeholderPattern.split("Error found while compiling: {}-{}", -1);
        return new CompiledStringFormat(messageParts, 30);
    }

	@Benchmark
    public CompiledStringFormat fastFormatCompileByStringSplit() {
        String[] messageParts = "Error found while compiling: {}-{}".split("\\{\\}", -1);
        return new CompiledStringFormat(messageParts, 30);
    }

	@Benchmark
    public CompiledStringFormat fastFormatCompileBySplitterSplit() {
	    String[] messageParts = placeholderSplitter.splitToStream("Error found while compiling: {}-{}").toArray(String[]::new);
        return new CompiledStringFormat(messageParts, 30);
    }

	@Benchmark
	public MessageFormat javaMessageFormat() {
		return new MessageFormat("Error found while compiling: {0}-{1}");
	}

	@Benchmark
	public MessageFormat javaMessageFormatSpecializedForNumber() {
		return new MessageFormat("Error found while compiling: {0}-{1,number,integer}");
	}

}
