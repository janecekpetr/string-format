package com.gitlab.janecekpetr.stringformat;

import com.google.common.base.Strings;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.tinylog.format.AdvancedMessageFormatter;
import org.tinylog.format.LegacyMessageFormatter;

import java.text.MessageFormat;
import java.util.Formatter;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Fork(5)
@Warmup(iterations = 2, time = 3, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 4, time = 3, timeUnit = TimeUnit.SECONDS)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class StringFormattingBenchmark {

    private static final int EXACT_CAPACITY = 29+10+1+10;

    private String string;
	private long number;

	// instead of String try a class with a toString

	@Setup
	public void setup(ScreenSleepDisablingBenchmark screenSleepDisabler) {
		string = "0123456789";
		number = 1_234_567_890L;
	}

    @State(Scope.Benchmark)
    public static class FormattersState {
        private CompiledStringFormat compiledStringFormat;
        private MessageFormat messageFormat;
        private MessageFormat messageFormatSpecializedForNumber;
        private org.tinylog.format.MessageFormatter tinyLogLegacyFormatter;
        private org.tinylog.format.MessageFormatter tinyLogAdvancedFormatter;
        private org.tinylog.format.MessageFormatter tinyLogAdvancedFormatterNoEscaping;

        @Setup
        public void setup() {
            compiledStringFormat = StringFormat.compile("Error found while compiling: {}-{}");
            messageFormat = new MessageFormat("Error found while compiling: {0}-{1}");
            messageFormatSpecializedForNumber = new MessageFormat("Error found while compiling: {0}-{1,number,integer}");
            tinyLogLegacyFormatter = new LegacyMessageFormatter();
            tinyLogAdvancedFormatter = new AdvancedMessageFormatter(Locale.ROOT, true);
            tinyLogAdvancedFormatterNoEscaping = new AdvancedMessageFormatter(Locale.ROOT, false);
        }
    }

	@Benchmark
	public String fastFormatStatic() {
		return StringFormat.format("Error found while compiling: {}-{}", string, number);
	}

    @Benchmark
    public String fastFormat(FormattersState stringFormatCache) {
        return stringFormatCache.compiledStringFormat.format(string, number);
    }

    @Benchmark
    public String fastFormatTo(FormattersState stringFormatCache) {
        StringBuilder stringBuilder = new StringBuilder();
        stringFormatCache.compiledStringFormat.formatTo(stringBuilder, string, number);
        return stringBuilder.toString();
    }

    @Benchmark
    public String fastFormatToSized(FormattersState stringFormatCache) {
        StringBuilder stringBuilder = new StringBuilder(EXACT_CAPACITY);
        stringFormatCache.compiledStringFormat.formatTo(stringBuilder, string, number);
        return stringBuilder.toString();
    }

    @Benchmark
    public String fastFormatWithVarArgs(FormattersState stringFormatCache) {
        return stringFormatCache.compiledStringFormat.format(new Object[] { string, number });
    }

    @Benchmark
    public String fastFormatToWithVarArgs(FormattersState stringFormatCache) {
        StringBuilder stringBuilder = new StringBuilder();
        stringFormatCache.compiledStringFormat.formatTo(stringBuilder, new Object[] { string, number });
        return stringBuilder.toString();
    }

    @Benchmark
    public String fastFormatToSizedWithVarArgs(FormattersState stringFormatCache) {
        StringBuilder stringBuilder = new StringBuilder(EXACT_CAPACITY);
        stringFormatCache.compiledStringFormat.formatTo(stringBuilder, new Object[] { string, number });
        return stringBuilder.toString();
    }

	@Benchmark
	public String stringPlus() {
		return "Error found while compiling: " + string + "-" + number;
	}

	@Benchmark
	public String stringBuilder() {
		return new StringBuilder()
	        .append("Error found while compiling: ")
			.append(string)
			.append('-')
			.append(number)
			.toString();
	}

	@Benchmark
	public String stringBuilderSized() {
		return new StringBuilder(EXACT_CAPACITY)
	        .append("Error found while compiling: ")
			.append(string)
			.append('-')
			.append(number)
			.toString();
	}

	@Benchmark
	public String stringBuffer() {
		return new StringBuffer()
	        .append("Error found while compiling: ")
			.append(string)
			.append('-')
			.append(number)
			.toString();
	}

	@Benchmark
	public String stringBufferSized() {
		return new StringBuffer(EXACT_CAPACITY)
	        .append("Error found while compiling: ")
			.append(string)
			.append('-')
			.append(number)
			.toString();
	}

	@Benchmark
	public String stringConcat() {
		return "Error found while compiling: "
	        .concat(string)
			.concat("-")
			.concat(Long.toString(number));
	}

	@Benchmark
	public String guavaLenientFormat() {
		return Strings.lenientFormat("Error found while compiling: %s-%s", string, number);
	}

	@Benchmark
	public String slf4jMessageFormatter() {
		return org.slf4j.helpers.MessageFormatter.format("Error found while compiling: {}-{}", string, number).getMessage();
	}

	@Benchmark
	public String slf4jMessageFormatterObjects() {
		return org.slf4j.helpers.MessageFormatter.arrayFormat("Error found while compiling: {}-{}", new Object[] { string, number }).getMessage();
	}

    @Benchmark
    public String log4jParameterizedMessage() {
        return ParameterizedMessage.format("Error found while compiling: {}-{}", new Object[] { string, number });
    }

    @Benchmark
    public String tinyLogLegacyFormatter(FormattersState state) {
        return state.tinyLogLegacyFormatter.format("Error found while compiling: {}-{}", new Object[] { string, number });
    }

    @Benchmark
    public String tinyLogAdvancedFormatter(FormattersState state) {
        return state.tinyLogAdvancedFormatter.format("Error found while compiling: {}-{}", new Object[] { string, number });
    }

    @Benchmark
    public String tinyLogAdvancedFormatterNoEscaping(FormattersState state) {
        return state.tinyLogAdvancedFormatterNoEscaping.format("Error found while compiling: {}-{}", new Object[] { string, number });
    }

	@Benchmark
	public String stringFormat() {
		return String.format("Error found while compiling: %s-%s", string, number);
	}

	@Benchmark
	public String stringFormatSpecializedForNumber() {
		return String.format("Error found while compiling: %s-%d", string, number);
	}

	@Benchmark
	public String javaFormatter() {
		try (Formatter formatter = new Formatter()) {
			return formatter.format("Error found while compiling: %s-%s", string, number).toString();
		}
	}

	@Benchmark
	public String javaFormatterSpecializedForNumber() {
		try (Formatter formatter = new Formatter()) {
			return formatter.format("Error found while compiling: %s-%d", string, number).toString();
		}
	}

	@Benchmark
	public String javaMessageFormat(FormattersState state) {
		return state.messageFormat.format(new Object[] {string, number});
	}

	@Benchmark
	public String javaMessageFormatSpecializedForNumber(FormattersState state) {
		return state.messageFormatSpecializedForNumber.format(new Object[] {string, number});
	}

}
