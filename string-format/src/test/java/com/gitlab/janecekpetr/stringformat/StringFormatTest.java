package com.gitlab.janecekpetr.stringformat;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class StringFormatTest {

    // TODO null/empty params

    static class SingleParameterTests {
        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormat(String formatString, String parameter, String expected) {
            String result = StringFormat.format(formatString, parameter);
            assertThat(result).isEqualTo(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatTo(String formatString, String parameter, String expected) {
            StringBuilder result = StringFormat.formatTo(new StringBuilder(), formatString, parameter);
            assertThat(result).asString().isEqualTo(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatCompiled(String formatString, String parameter, String expected) {
            String result = StringFormat.compile(formatString)
                .format(parameter);
            assertThat(result).isEqualTo(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatToCompiled(String formatString, String parameter, String expected) {
            StringBuilder result = StringFormat.compile(formatString)
                .formatTo(new StringBuilder(), parameter);
            assertThat(result).asString().isEqualTo(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatCompiledWithVarArgs(String formatString, String parameter, String expected) {
            String result = StringFormat.compile(formatString)
                .format(new Object[] {parameter});
            assertThat(result).isEqualTo(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatToCompiledWithVarArgs(String formatString, String parameter, String expected) {
            StringBuilder result = StringFormat.compile(formatString)
                .formatTo(new StringBuilder(), new Object[] {parameter});
            assertThat(result).asString().isEqualTo(expected);
        }

        static Stream<Arguments> parametersSource() {
            return Stream.of(
                    arguments("{}", "", ""),
                    arguments("{}", "{}", "{}"),

                    arguments("{}", "sole", "sole"),
                    arguments("{}!", "start", "start!"),
                    arguments("!{}!", "middle", "!middle!"),
                    arguments("!{}", "end", "!end"),

                    arguments("\\{}", "no escaping", "\\no escaping"),

                    arguments("{} {", "partial end", "partial end {"),
                    arguments("{ {}", "partial start", "{ partial start")
            );
        }
    }

    static class IncorrectNumberOfParametersTests {
        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormat(String formatString, String parameter, Class<? extends RuntimeException> expected) {
            assertThatThrownBy(
                    () -> StringFormat.format(formatString, parameter)
                ).isInstanceOf(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatTo(String formatString, String parameter, Class<? extends RuntimeException> expected) {
            assertThatThrownBy(
                    () -> StringFormat.formatTo(new StringBuilder(), formatString, parameter)
                ).isInstanceOf(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatCompiled(String formatString, String parameter, Class<? extends RuntimeException> expected) {
            assertThatThrownBy(
                    () -> StringFormat.compile(formatString).format(parameter)
                ).isInstanceOf(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatToCompiled(String formatString, String parameter, Class<? extends RuntimeException> expected) {
            assertThatThrownBy(
                    () -> StringFormat.compile(formatString).formatTo(new StringBuilder(), parameter)
                ).isInstanceOf(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatCompiledWithVarArgs(String formatString, String parameter, Class<? extends RuntimeException> expected) {
            assertThatThrownBy(
                    () -> StringFormat.compile(formatString).format(new Object[] {parameter})
                ).isInstanceOf(expected);
        }

        @ParameterizedTest
        @MethodSource("parametersSource")
        public void testFormatToCompiledWithVarArgs(String formatString, String parameter, Class<? extends RuntimeException> expected) {
            assertThatThrownBy(
                    () -> StringFormat.compile(formatString).formatTo(new StringBuilder(), new Object[] {parameter})
                ).isInstanceOf(expected);
        }

        static Stream<Arguments> parametersSource() {
            return Stream.of(
                    arguments(null, "", NullPointerException.class),
                    arguments("", null, NullPointerException.class),

                    arguments("{}", "sole", "sole"),
                    arguments("{}!", "start", "start!"),
                    arguments("!{}!", "middle", "!middle!"),
                    arguments("!{}", "end", "!end"),

                    arguments("\\{}", "no escaping", "\\no escaping"),

                    arguments("{} {", "partial end", "partial end {"),
                    arguments("{ {}", "partial start", "{ partial start")
            );
        }
    }

}
