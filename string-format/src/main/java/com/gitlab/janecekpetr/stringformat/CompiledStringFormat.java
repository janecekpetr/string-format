// Copyright (c) 2004-2022 QOS.ch, Petr Janeček
// All rights reserved.
//
// Permission is hereby granted, free  of charge, to any person obtaining
// a  copy  of this  software  and  associated  documentation files  (the
// "Software"), to  deal in  the Software without  restriction, including
// without limitation  the rights to  use, copy, modify,  merge, publish,
// distribute,  sublicense, and/or sell  copies of  the Software,  and to
// permit persons to whom the Software  is furnished to do so, subject to
// the following conditions:
//
// The  above  copyright  notice  and  this permission  notice  shall  be
// included in all copies or substantial portions of the Software.
//
// THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
// EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
// MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.gitlab.janecekpetr.stringformat;

import java.util.Arrays;
import javax.annotation.processing.Generated;

@Generated(
		value = "com.squareup.javapoet:1.13.0",
		date = "2022-07-08T10:30:33.092823+02:00"
)
public final class CompiledStringFormat {
	private static final int NUMBER_TOSTRING_LENGTH_GUESS = 10;

	private static final int OBJECT_TOSTRING_LENGTH_GUESS = 16;

	private final String[] messageParts;

	private final int length;

	CompiledStringFormat(String[] messageParts, int length) {
		this.messageParts = messageParts;
		this.length = length;
	}

	public String format(Object... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*OBJECT_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(String... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*OBJECT_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(int... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(long... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(double... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(byte... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(char... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(boolean... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(float... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public String format(short... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		switch (argCount) {
			case 1: return format(args[0]);
			case 2: return format(args[0], args[1]);
			case 3: return format(args[0], args[1], args[2]);
			case 4: return format(args[0], args[1], args[2], args[3]);
			default: {
				StringBuilder builder = new StringBuilder(length + argCount*NUMBER_TOSTRING_LENGTH_GUESS);
				formatTo(builder, args);
				return builder.toString();
			}
		}
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			ToStrings.deepAppendObject(stringBuilder, args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, byte... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, char... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, boolean... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, float... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, short... args) {
		final int argCount = args.length;
		if (messageParts.length != (argCount + 1)) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: " + Arrays.toString(args));
		}
		for (int i = 0; i < argCount; i++) {
			stringBuilder.append(messageParts[i]);
			stringBuilder.append(args[i]);
		}
		stringBuilder.append(messageParts[argCount]);
		return stringBuilder;
	}

	public String format(Object arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1];
	}

	public String format(String arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1];
	}

	public String format(int arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1];
	}

	public String format(long arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1];
	}

	public String format(double arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1];
	}

	public String format(Object arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2];
	}

	public String format(Object arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(Object arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(Object arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(Object arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(String arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2];
	}

	public String format(String arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(String arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(String arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(String arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(int arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2];
	}

	public String format(int arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(int arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(int arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(int arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(long arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2];
	}

	public String format(long arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(long arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(long arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(long arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(double arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2];
	}

	public String format(double arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(double arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(double arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(double arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2];
	}

	public String format(Object arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(Object arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(Object arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(Object arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(Object arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(Object arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(String arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(String arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(String arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(String arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(String arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(String arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(int arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(int arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(int arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(int arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(int arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(int arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(long arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(long arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(long arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(long arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(long arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(long arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(double arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(double arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(double arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(double arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3];
	}

	public String format(double arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(double arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3];
	}

	public String format(Object arg0, Object arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, Object arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, Object arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, Object arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, Object arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, Object arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, Object arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, String arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, String arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, String arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, String arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, String arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, String arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, int arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, int arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, int arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, int arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, int arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, int arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, long arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, long arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, long arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, long arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, long arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, long arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, double arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, double arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, double arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, double arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(Object arg0, double arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(Object arg0, double arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + ToStrings.deepToString(arg0) + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, Object arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, Object arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, Object arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, Object arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, Object arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, Object arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, String arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, String arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, String arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, String arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, String arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, String arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, int arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, int arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, int arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, int arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, int arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, int arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, long arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, long arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, long arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, long arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, long arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, long arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, double arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, double arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, double arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, double arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(String arg0, double arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(String arg0, double arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, Object arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, Object arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, Object arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, Object arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, Object arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, Object arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, String arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, String arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, String arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, String arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, String arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, String arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, int arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, int arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, int arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, int arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, int arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, int arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, long arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, long arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, long arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, long arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, long arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, long arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, double arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, double arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, double arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, double arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(int arg0, double arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(int arg0, double arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, Object arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, Object arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, Object arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, Object arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, Object arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, Object arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, String arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, String arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, String arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, String arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, String arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, String arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, int arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, int arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, int arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, int arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, int arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, int arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, long arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, long arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, long arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, long arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, long arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, long arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, double arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, double arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, double arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, double arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(long arg0, double arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(long arg0, double arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, Object arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, Object arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, Object arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, Object arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, Object arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, Object arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + ToStrings.deepToString(arg1) + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, String arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, String arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, String arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, String arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, String arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, String arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, int arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, int arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, int arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, int arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, int arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, int arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, long arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, long arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, long arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, long arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, long arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, long arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, Object arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, double arg1, Object arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, Object arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, Object arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, Object arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + ToStrings.deepToString(arg2) + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, String arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, double arg1, String arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, String arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, String arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, String arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, int arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, double arg1, int arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, int arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, int arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, int arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, long arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, double arg1, long arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, long arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, long arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, long arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, double arg2, Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + ToStrings.deepToString(arg3) + messageParts[4];
	}

	public String format(double arg0, double arg1, double arg2, String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, double arg2, int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, double arg2, long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public String format(double arg0, double arg1, double arg2, double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		return messageParts[0] + arg0 + messageParts[1] + arg1 + messageParts[2] + arg2 + messageParts[3] + arg3 + messageParts[4];
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0) {
		if (messageParts.length != 2) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1) {
		if (messageParts.length != 3) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, Object arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, String arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, int arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, long arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, double arg2) {
		if (messageParts.length != 4) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, Object arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, String arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, int arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, long arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, Object arg0, double arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		ToStrings.deepAppendObject(stringBuilder, arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, Object arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, String arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, int arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, long arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, String arg0, double arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, Object arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, String arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, int arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, long arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, int arg0, double arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, Object arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, String arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, int arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, long arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, long arg0, double arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, Object arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		ToStrings.deepAppendObject(stringBuilder, arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, String arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, int arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, long arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, Object arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, Object arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, Object arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, Object arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, Object arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		ToStrings.deepAppendObject(stringBuilder, arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, String arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, String arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, String arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, String arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, String arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, int arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, int arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, int arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, int arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, int arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, long arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, long arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, long arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, long arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, long arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, double arg2,
			Object arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		ToStrings.deepAppendObject(stringBuilder, arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, double arg2,
			String arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, double arg2,
			int arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, double arg2,
			long arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}

	public StringBuilder formatTo(StringBuilder stringBuilder, double arg0, double arg1, double arg2,
			double arg3) {
		if (messageParts.length != 5) {
			throw new IllegalArgumentException("Incorrect number of arguments, message parts: " + Arrays.toString(messageParts) + ", args: [" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + "]");
		}
		stringBuilder.append(messageParts[0]);
		stringBuilder.append(arg0);
		stringBuilder.append(messageParts[1]);
		stringBuilder.append(arg1);
		stringBuilder.append(messageParts[2]);
		stringBuilder.append(arg2);
		stringBuilder.append(messageParts[3]);
		stringBuilder.append(arg3);
		stringBuilder.append(messageParts[4]);
		return stringBuilder;
	}
}
