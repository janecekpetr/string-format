// Copyright (c) 2004-2022 QOS.ch, Petr Janeček
// All rights reserved.
//
// Permission is hereby granted, free  of charge, to any person obtaining
// a  copy  of this  software  and  associated  documentation files  (the
// "Software"), to  deal in  the Software without  restriction, including
// without limitation  the rights to  use, copy, modify,  merge, publish,
// distribute,  sublicense, and/or sell  copies of  the Software,  and to
// permit persons to whom the Software  is furnished to do so, subject to
// the following conditions:
//
// The  above  copyright  notice  and  this permission  notice  shall  be
// included in all copies or substantial portions of the Software.
//
// THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
// EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
// MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.gitlab.janecekpetr.stringformat;

import java.util.ArrayList;
import java.util.List;

public class StringFormat {

	private static final int OBJECT_LENGTH_GUESS = 16;
    private static final String PLACEHOLDER = "{}";
    private static final int PLACEHOLDER_LENGTH = PLACEHOLDER.length();

    private StringFormat() {
    	// A static utility class.
    }

    public static String format(String formatString, Object... args) {
        if ((formatString == null) || (args == null) || (args.length == 0)) {
            // TODO Should we throw here?
            return formatString;
        }

        StringBuilder stringBuilder = new StringBuilder(formatString.length() + (OBJECT_LENGTH_GUESS * args.length));
        return formatTo(stringBuilder, formatString, args)
            .toString();
    }

    public static StringBuilder formatTo(StringBuilder stringBuilder, String formatString, Object... args) {
        if ((formatString == null) || (args == null) || (args.length == 0)) {
            // TODO Should we throw here?
            return stringBuilder;
        }

        int argIndex = 0;

        int currentIndex = 0;
        int placeholderIndex = formatString.indexOf(PLACEHOLDER);
        while (placeholderIndex != -1) {
            stringBuilder.append(formatString, currentIndex, placeholderIndex)
                .append(ToStrings.deepToString(args[argIndex++]));
            currentIndex = placeholderIndex + PLACEHOLDER_LENGTH;
            placeholderIndex = formatString.indexOf(PLACEHOLDER, currentIndex);
        }

        // Append the characters following the last {} pair.
        stringBuilder.append(formatString, currentIndex, formatString.length());

        return stringBuilder;
    }

    // TODO Maybe indicate whether the first and/or last messagePart is empty?
    public static CompiledStringFormat compile(String formatString) {
        List<String> messageParts = new ArrayList<>();

        int currentIndex = 0;
        int placeholderIndex = formatString.indexOf(PLACEHOLDER);
        while (placeholderIndex != -1) {
            String part = formatString.substring(currentIndex, placeholderIndex);
            messageParts.add(part);

            currentIndex = placeholderIndex + PLACEHOLDER_LENGTH;
            placeholderIndex = formatString.indexOf(PLACEHOLDER, currentIndex);
        }

        String lastPart = formatString.substring(currentIndex);
        messageParts.add(lastPart);

        int placeholdersFound = messageParts.size() - 1;
        int formatStringLengthWithoutPlaceholders = formatString.length() - (placeholdersFound * PLACEHOLDER_LENGTH);
        return new CompiledStringFormat(messageParts.toArray(String[]::new), formatStringLengthWithoutPlaceholders);
    }

}