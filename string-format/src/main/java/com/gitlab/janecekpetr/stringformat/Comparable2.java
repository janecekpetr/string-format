package com.gitlab.janecekpetr.stringformat;

    @FunctionalInterface
    interface Comparable2<T> extends Comparable<T> {

        public default boolean lessThan(T other) {
            return compareTo(other) < 0;
        }

        public default boolean lessThanOrEqual(T other) {
            return compareTo(other) <= 0;
        }

        public default boolean comparesEqual(T other) {
            return compareTo(other) == 0;
        }

        public default boolean greaterThanOrEqual(T other) {
            return compareTo(other) >= 0;
        }

        public default boolean greaterThan(T other) {
            return compareTo(other) > 0;
        }

    }

    @FunctionalInterface
    interface Comparable3<T> extends Comparable<T> {

        public enum ComparisonResult {
            LESS {
                @Override
                public boolean isLess() {
                    return true;
                }

                @Override
                public boolean isLessOrEqual() {
                    return true;
                }

                @Override
                public boolean isEqual() {
                    return false;
                }

                @Override
                public boolean isGreaterOrEqual() {
                    return false;
                }

                @Override
                public boolean isGreater() {
                    return false;
                }
            },

            EQUAL {
                @Override
                public boolean isLess() {
                    return false;
                }

                @Override
                public boolean isLessOrEqual() {
                    return true;
                }

                @Override
                public boolean isEqual() {
                    return true;
                }

                @Override
                public boolean isGreaterOrEqual() {
                    return true;
                }

                @Override
                public boolean isGreater() {
                    return false;
                }
            },

            GREATER {
                @Override
                public boolean isLess() {
                    return false;
                }

                @Override
                public boolean isLessOrEqual() {
                    return false;
                }

                @Override
                public boolean isEqual() {
                    return false;
                }

                @Override
                public boolean isGreaterOrEqual() {
                    return true;
                }

                @Override
                public boolean isGreater() {
                    return true;
                }
            };

            public abstract boolean isLess();
            public abstract boolean isLessOrEqual();
            public abstract boolean isEqual();
            public abstract boolean isGreaterOrEqual();
            public abstract boolean isGreater();
        }

        public default ComparisonResult comparedTo(T other) {
            int result = Integer.signum(compareTo(other));
            return switch (result) {
                case -1 -> ComparisonResult.GREATER;
                case 0 -> ComparisonResult.EQUAL;
                case 1 -> ComparisonResult.LESS;
                default -> throw new AssertionError("Unexpected value from Integer.signum(): " + result);
            };
        }
    }

    @FunctionalInterface
    interface Comparable4<T> extends Comparable<T> {

        public record ComparisonResult<R>(R left, R right, int comparisonResult) {

            public boolean isLess() {
                return (comparisonResult < 0);
            }

            public boolean isLessOrEqual() {
                return (comparisonResult <= 0);
            }

            public boolean isEqual() {
                return (comparisonResult == 0);
            }

            public boolean isGreaterOrEqual() {
                return (comparisonResult >= 0);
            }

            public boolean isGreater() {
                return (comparisonResult > 0);
            }

        }

        @SuppressWarnings("unchecked")
        public default ComparisonResult<T> comparedTo(T other) {
            return new ComparisonResult<>((T)this, other, this.compareTo(other));
        }

    }
